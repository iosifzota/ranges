#pragma once

#include "summer_school/range/category.hpp"

namespace summer_school::range::pipe {
	using summer_school::range::category::is_pipe_head;
	using summer_school::range::category::is_range_factory;

// IV. Now it's time to define a way to compose ranges togheter. Technically, you can use any binary operator with left
// to right associativity but we recommend operator|. If you are familliar with linux this is also the way bash
// commands are composed in the terminal. The behavior is similar to ranges. The left operand can be either a
// container, a generator or a view, therefore you can use is_pipe_head to check the type. The right operand must be a
// factory, likewise, use is_range_factory to do the check. This will require SFINAE and you can use std::enable_if_t.
//
// Although we can use std::vector as a container we currently don't have any views or reductions compose it with.
// You'll do this kind of testing in pipe.test.cpp later. For now go to integers.hpp.
template<typename InputRange, typename RangeFactory, typename = std::enable_if_t< is_pipe_head<InputRange> && is_range_factory<RangeFactory> > >
auto operator|(InputRange&& inputRange, RangeFactory&& rangeFactory) {
	return rangeFactory.create(std::forward<InputRange>(inputRange));
}

}
