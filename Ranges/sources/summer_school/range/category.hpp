#pragma once

#include <type_traits>

// Welcome! This is the beginning of your journey through templates land! There are many challenges ahead so don't
// hesitate to raise your hand if you get stuck. Templates are one of the most complex features of C++ so don't
// get discouraged if you don't get it the first time. Like anything in life you'll get better with practice.
namespace summer_school::range::category {

struct container_tag {};

struct generator_tag {};

struct view_tag {};

struct reduction_tag {};

struct view_factory_tag {};

struct reduction_factory_tag {};

// I. First we need a way to check if a type is either a container, a generator or a view. We'll use this in the pipe
// operator to check if we are composing the right types. These ranges are the head and the middle parts of the pipe,
// hence the name pipe head. Notice that the pipe head can't contain reductions. These are used to close the pipe. You
// can do the check either by using base classes and std::is_base_of, or you can you use SFINAE and member types. We
// recommend the later so you can tread vectors as containers more easily but also to get a better intuition on
// templates.
struct general_case {};
struct best_case : general_case {};

template<typename Type>
struct has_tag
{
	template<bool = false>
	static constexpr auto get_value(general_case) { return false; }

	template<bool = std::is_class_v<Type::range_category>>
	static constexpr auto get_value(best_case) { return true; }
};

template<typename Type>
inline constexpr auto has_tag_v = has_tag<std::decay_t<Type>>::get_value(best_case());

template<typename Type, typename = void>
struct pipe_head
{
	static constexpr auto get_value(general_case) { return false; }
};

template<typename Type>
struct pipe_head<Type, std::enable_if_t<has_tag_v<Type>>>
{
	template<bool = false>
	static constexpr auto get_value(general_case) { return true; }

	template<bool = std::is_same_v<reduction_tag, decltype(Type::range_category)>>
	static constexpr auto get_value(best_case) { return false; }
};

template<typename T>
struct pipe_head<std::vector<T>>
{
	static constexpr auto get_value(general_case) { return true; }
};

template<typename Type>
inline constexpr auto is_pipe_head = pipe_head<std::decay_t<Type>>::get_value(best_case());

// II. Besides the range itself we also need factories. Without them we cannot compose the output of a range to the
// input of another range. is_range_factory makes sure we have the correct type in place. Only views and reductions
// have factories because containers and generators don't have an input range. Therefore a pipe always begins with
// either a container or a generator. If you previously used SFINAE we recommend std::is_base_of this time to cover
// both methods. Next we'll test this module in the category.test.cpp file.
template<typename Type, typename Decayed = std::decay_t<Type>>
inline constexpr bool is_range_factory = std::is_base_of_v<view_factory_tag, Decayed> || std::is_base_of_v<reduction_factory_tag, Decayed>;

}
