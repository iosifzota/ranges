#pragma once

#include "summer_school/range/category.hpp"
#include <iterator>

namespace summer_school::range::integers {

// V. Time to get down to business. We'll implement a range that will generate integers in arithmetic order. For
// example integers() will generate an "infinite" series of integers starting with zero. integers(1, 2) will generate
// odd integers starting with 1. integers(0, 5, 20) wil generate 20 multiples of 5 starting with 0. In order to
// implement a generator you'll need an iterator, a range and function that will return the range.
//
// If you are familliar with C++ iterators you probably know there are different categories of iterators depending on
// what operations they have. Luckily, range iterators require the most simple category and that is the InputIterator.
// This operator has the following operators:
// - pre and post incrementation, operator++, goes to the next integer in the series
// - dereferencing, operator*, returns an integer
// - equality check, operator== and operator!=
// Note: In order to use your iterators with std algorithms you need to provide the iterator traits value_type, pointer
// reference, difference_type and iterator_category.
class IntegersIterator {
public:
    /* YOUR CODE HERE */
	using iterator_category = std::input_iterator_tag;

	IntegersIterator(int init, int leap, int end) : m_value{ init }, m_leap{ leap }, m_end{end}
	{
		// empty
	}

	auto& operator++()
	{
		if (m_value != m_end)
		{
			m_value += m_leap;
		}
		return *this;
	}

	auto operator++(int)
	{
		auto copy = *this;
		++*this;
		return copy;
	}

	auto operator*()
	{
		return m_value;
	}

	friend bool operator==(const IntegersIterator& lhs, const IntegersIterator& rhs)
	{
		return lhs.m_value == rhs.m_value;
	}

	friend bool operator!=(const IntegersIterator& lhs, const IntegersIterator& rhs)
	{
		return !(lhs == rhs);
	}

	friend class IntegersGenerator;
private:
	int m_value, m_leap, m_end;
};

// VI. The integers generator is a range and like any range it defines two methods, begin() and end(), returning an
// iterator with the first integer in the sequence and an iterator with the last integer respectively. Don't forget to
// tag it using your preferred method so you can use it with the pipe operator|.
class IntegersGenerator {
public:
    /* YOUR CODE HERE */
	using range_category = category::generator_tag;
	using const_iterator = IntegersIterator;
	using iterator = const_iterator;

	IntegersGenerator(int start, int leap, int count) :
		m_start{ start }, m_leap{ leap }, m_count{ count }
	{
		// empty
	}

	IntegersGenerator(int start, int leap) :
		m_start{ start }, m_leap{ leap }
	{
		m_count = -1;
	}

	IntegersIterator begin() const
	{
		return IntegersIterator{ m_start, m_leap, get_end() };
	}

	const IntegersIterator& end() const
	{
		static IntegersIterator endItr{ get_end(), 0, 0 };
		return endItr;
	}

	int get_end() const
	{
		if (-1 == m_count)
		{
			return m_start - 1;
		}

		long max = (long)m_start + (long)m_leap * (long)m_count;
		int maxInt = std::numeric_limits<int>::max();

		return (max > maxInt) ? maxInt : (int)max;
	}

private:
	int m_start, m_leap, m_count;
};

// VII. This function returns the IntegersGenerator. Don't forget to provide default values for the parameters. The
// function is merely sugar coating so when we chain ranges we don't have to worry about the underlying type of the
// range. We let the pipe operator| solve the type for us through template parameter deduction.
// Next is integers.test.cpp.
inline auto integers(int start, int step = 1, int count = -1) { /* YOUR CODE HERE */
	return IntegersGenerator(start, step, count);
}

}
