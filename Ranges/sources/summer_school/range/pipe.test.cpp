#include "catchorg/catch/catch.hpp"
#include "summer_school/range/pipe.hpp"
#include "summer_school/range/fold.hpp"
#include "summer_school/range/integers.hpp"

using summer_school::range::pipe::operator|;
using summer_school::range::fold::fold;
using summer_school::range::integers::integers;

int sum(int a, int b)
{
	return a + b;
}

// XVII. Here you'll test different combinations of containers, generators, views and reductions. After this
// you are done! Congratulations!
TEST_CASE("Pipe") { /* YOUR CODE HERE */

	const int count = 10;
	int res = integers(0, 1, count) | fold(0, sum);
	
	auto vals = integers(0, 1, count);

	int result = 0;
	for (const auto& itr : vals)
	{
		result = sum(result, itr);
	}

	REQUIRE(res == result);
}
