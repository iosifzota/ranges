#include <iostream>
#include <stdexcept>

#include "summer_school/pointer_project/vector.hpp"

#include <algorithm>

namespace summer_school::pointer_project {

// ########################
// # Super secret project #
// ########################
// Your task is to write an implementation for an array of doubles. But this is not a simple C-style array. Recall that
// C-style arrays are fixed in size. Once you pass the array size you can no longer add elements. In practice this is a
// big downside. When we store data we may not know how big of an array we need so we need something more flexible.
// Hence this project, we'll implement an array that can grow in size as needed. To make the distinction from a normal
// array we'll call it a vector.
//
// This project already has code written by us. It has basic functionality so you can focus on the important stuff.
// Your job is to write the implementation for several methods of the Vector class. You may not know many things about
// classes but don't worry, writing the code is more easy than it sounds. Also, if you get stuck raise your hand and
// the closest trainer or helper will be quickly dispatched to your location. There are no wrong questions. No need to
// be shy!
//
// In order to easily track your progress and make sure your code works we've written a bunch of tests that run
// automatically when you execute the compiled project. You can compile and run the project right now to get an
// overview of the tests.
//
// As you can see there are a lot of failed tests. Don't worry, at the end of the day everything will be green! Notice
// that tests are numbered. These represent tasks, more precisely methods, that you need to implement. Once all tests
// pass you are done!
//
// As you can see the implementation we provided generates errors. There are multiple sections in this file where you
// need to write your own implementation. We've marked these sections with 'YOUR CODE HERE'. These are the only places
// were you need to write code. Feel free to view the other files but please do not modify them.
// Okay, let's get down to business!

// 1. Capacity: This method returns the number of elements the vector can currently hold without allocating new memory,
// making room for more elements. It's different than the size of the vector. For example the vector can have enough
// space for 4 elements but none constructed in memory, therefore its size is zero. You may ask is this necessary?
// The answer is yes. A reason is performance. If capacity was always equal to the size then we would have to allocate
// memory each time we add an element to the vector. Okay, performance is nice to have but optional. After all this is
// a summer school exercise, let's not go overboard with performance. But even so there's a second and a better reason.
// What if we erase one element from the end let's say? We would end up with an empty place therefore the size will be
// less than the capacity. We could enforce the vector to always have constructed elements but this complicates the
// code.
int Vector::capacity() const { return capacity_ - begin_; }

// 2. Size: Returns the number of elements the vector is currently holding. This should always be less than or equal
// to capacity.
int Vector::size() const { return end_ - begin_; }

// 3. Subscript operator: This allows the vector to be indexed as a typical C-style array, e.g. vector[2] to get the
// third element. Operators are just functions that can be called with a different syntax. We could have named it
// 'elementAtIndex' in which case we call it as vector.elementAtIndex(2) instead of vector[2].
// When writing functions or methods it's a good idea to think about valid and invalid arguments. In this case you
// should generate an error if the index is out of bounds. You can generate errors with this statement
// 'throw Oopsie{};'. Don't worry, generating errors for the right reasons won't fail your tests. We actually have
// tests that expect Oopsies. These are called negative tests and their purpose is to check the behavior of functions
// with invalid arguments.
double& Vector::operator[](int index) {
    if (0 <= index && index < size())
        return begin_[index];
    throw Oopsie{};
}

// 4. Push back: Things are getting interesting now. When we create an vector object, begin_, end_ and capacity_ will
// be all equal to nullptr. So from the start we have no capacity and we need to allocate memory. We also need to think
// about the general case. We can push back an element no matter the vector size. Food for thought:
// - how do we check if there is enough space for an element
// - if there is no more room available we need to allocate a new block of memory and copy our elements over there
void Vector::push_back(double element) {
    if (end_ == capacity_) {
        auto sameSize = size();
        auto newCapacityCount = std::max(1, capacity() * 2);
        auto newBegin = SafeAllocator::allocate(newCapacityCount);
        std::copy(begin_, end_, newBegin);
        SafeAllocator::deallocate(begin_);
        begin_ = newBegin;
        end_ = newBegin + sameSize;
        capacity_ = newBegin + newCapacityCount;
    }
    *end_++ = element;
}

// 5. Pop back: Popping elements is a lot easier than pushing them. We don't need to allocate new memory. But there is
// a case when a pop operation is illegal.
void Vector::pop_back() {
    if (size())
        --end_;
    else
        throw Oopsie{};
}

// 6. Insert: Wouldn't it be lovely if we could copy a C-style array into our vector? First and last point to a range in
// an array. In C++ the convention is to exclude the last element. In other words, we copy the interval [first, last).
// We will copy this range starting at position in our vector. Be careful, position should be a valid address in our
// vector memory block, and first and last should come from the same array. There some oopsies you have to cover for
// invalid positions.
void Vector::insert(pointer_type position, const double* first, const double* last) {
    if (position < begin_ || position > end_)
        throw Oopsie{};

    if (first != last) {
        auto newCapacityCount = (last - first) + size();
        auto newBegin = SafeAllocator::allocate(newCapacityCount);
        auto newEnd = std::copy(begin_, position, newBegin);
        newEnd = std::copy(first, last, newEnd);
        newEnd = std::copy(position, end_, newEnd);
        SafeAllocator::deallocate(begin_);
        begin_ = newBegin;
        end_ = newEnd;
        capacity_ = newBegin + newCapacityCount;
    }
}

// 7. Erase: Just like push and pop, insert and erase come together. We want to erase the elements [first, last) from
// the vector. First and last should be an subinterval from the vector memory block. Like before, be sure to validate
// the arguments. You won't pass all the tests if you don't.
void Vector::erase(pointer_type first, pointer_type last) {
    if (first < begin_ || first > end_ || last < begin_ || last > end_)
        throw Oopsie{};

    end_ = std::copy(last, end_, first);
}

// 8. Destructor: I have bad news. You've been leaking memory all over the place this entire time! Don't worry, we
// won't tell anybody. What happens in Brasov stays in Brasov. Jokes aside, it's time to do some clean up. The
// destructor is called when the object is no longer needed and it's disposed of. This ensures that any memory we
// allocate will be properly deallocated. Also, if you leak memory you'll find out. We've made sure of that ;)
Vector::~Vector() { SafeAllocator::deallocate(begin_); }

}
