#pragma once

#include "summer_school/range/category.hpp"
#include "summer_school/range/storage.hpp"

namespace summer_school::range::take {

// IX. Here you'll implement your first view. The view is the most composable of all the range category types. It can
// recieve an arbitrary range as input and its output can be binded to another range. Sound complex but the concept is
// simple. Because views offer the full package, they require an iterator, a range, a factory and a function that
// returns the factory. Unlike generators you cannot simply return the range directly because only the pipe operator|
// has the input range. You don't have the input range in the take function.

// Just like the integers generator we have to implement the same iteratornoperators. Take, well, takes the first n
// elements from the input range. Given iterator = takeView.begin(), after n increments of iterator, iterator must be
// equal to takeView.end(). The dereference operator simply returns the deference of the underlying input iterator.
template<typename Iterator>
class TakeIterator {
    /* YOUR CODE HERE */
};

// X. Like any range TakeView implements begin and end each returning a TakeIterator. Contrary to a generator you need
// to store the input range inside the view. This way ranges can compose. Use the Store class already provided in
// storage.hpp to do so.
template<typename InputRange>
class TakeView {
    /* YOUR CODE HERE */
};

// XI. Up till now you implemented the same concepts a generator had. This is where views stand apart. They need
// factories. Recall that factories take input range and binds it to the view. In other words you need to implement one
// method, create, which forwards the input range to a TakeView. Note that you also need to store the number of
// elements to take.
class TakeViewFactory {
    /* YOUR CODE HERE */
};

// XII. Return a TakeViewFactory. Okay, let's open take.test.cpp and test this.
inline auto take(int elements) { /* YOUR CODE HERE */
}

}
